//
//  CKCFlatButton.swift
//  CKCComponents
//
//  Created by Diego on 8/20/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

public class CKCFlatButton: CKCButton {
    
    public convenience init(title: String,
                            backgroundColor: UIColor,
                            textColor: UIColor = .white,
                            font: UIFont = .boldSystemFont(ofSize: 15),
                            cornerStyle: CornerStyle = .value(value: 8),
                            type: ButtonType = .system) {
        self.init(title: title, color: textColor, font: font, type: type, cornerStyle: cornerStyle)
        self.backgroundColor = backgroundColor
    }
    
    public convenience init(title: NSAttributedString,
                            backgroundColor: UIColor,
                            textColor: UIColor = .white,
                            font: UIFont = .boldSystemFont(ofSize: 15),
                            cornerStyle: CornerStyle = .value(value: 8),
                            type: ButtonType = .system) {
        self.init(title: title, color: textColor, font: font, type: type, cornerStyle: cornerStyle)
        self.backgroundColor = backgroundColor
    }
    
    private func setColors(_ primary: UIColor, secondary: UIColor) {
        self.backgroundColor = primary
        self.setTitleColor(secondary, for: .normal)
    }
    
}
