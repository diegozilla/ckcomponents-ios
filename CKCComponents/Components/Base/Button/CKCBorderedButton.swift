//
//  CKCBorderedButton.swift
//  CKCComponents
//
//  Created by Diego on 8/20/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

public class CKCBorderedButton: CKCButton {

    public convenience init(title: String,
                            borderColor: UIColor,
                            font: UIFont = .systemFont(ofSize: 14),
                            cornerStyle: CornerStyle = .value(value: 8),
                            borderWidth: CGFloat = 1,
                            type: ButtonType = .system) {
        self.init(title: title, color: borderColor, font: font, type: type, cornerStyle: cornerStyle)
        setBorder(color: borderColor, width: borderWidth)
    }
    
    public convenience init(title: NSAttributedString,
                            borderColor: UIColor,
                            font: UIFont = .systemFont(ofSize: 14),
                            cornerStyle: CornerStyle = .value(value: 8),
                            borderWidth: CGFloat = 1,
                            type: ButtonType = .system) {
        self.init(title: title, color: borderColor, font: font, type: type, cornerStyle: cornerStyle)
        setBorder(color: borderColor, width: borderWidth)
    }

    
    private func setBorder(color: UIColor, width: CGFloat) {
        layer.borderWidth = width
        layer.borderColor = color.cgColor
    }
}
