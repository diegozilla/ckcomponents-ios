//
//  CKCButton.swift
//  CKCComponents
//
//  Created by Diego on 7/28/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit
import CKFoundation

public class CKCButton: UIButton {

    public enum CornerStyle {
        case percentage(percent: CGFloat)
        case value(value: CGFloat)
        case none
    }
    
    private var cornerStyle: CornerStyle = .none
    private lazy var onTap = CKFMessage.Passthrough<CKCButton>()
    
    public convenience init(title: NSAttributedString,
                            color: UIColor,
                            font: UIFont = .boldSystemFont(ofSize: 15),
                            type: ButtonType = .system,
                            cornerStyle: CornerStyle = .none) {
        self.init(type: type)
        setAttributedTitle(title, for: .normal)
        self.cornerStyle = cornerStyle
        self.titleLabel?.font = font
        setTitleColor(color, for: .normal)
        initialize()
    }
    
    public convenience init(title: String,
                            color: UIColor,
                            font: UIFont = .boldSystemFont(ofSize: 15),
                            type: ButtonType = .system,
                            cornerStyle: CornerStyle = .none) {
        self.init(type: type)
        setTitle(title, for: .normal)
        self.cornerStyle = cornerStyle
        self.titleLabel?.font = font
        setTitleColor(color, for: .normal)
        initialize()
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        switch cornerStyle {
        case .none: break
        case .value(let value): layer.cornerRadius = value
        case .percentage(let percent):
            let value = percent < 0 ? 0 : percent > 1 ? 1 : percent
            layer.cornerRadius = value
        }
    }
 
    public func initialize() {
        addTarget(self, action: #selector(action(button:)), for: .touchUpInside)
    }

    @objc private func action(button: CKCButton) {
        self.onTap.callback?(button)
    }
    
    public func onTap<Binder: AnyObject>(binder: Binder, _ callback: @escaping (Binder, CKCButton) -> Void) {
        onTap.observe(binder, with: callback)
    }
}
