//
//  CKCSegmentedControl.swift
//  CKCComponents
//
//  Created by Diego on 12/2/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit
import CKFoundation

public class CKCSegmentedControl: UISegmentedControl {

    fileprivate lazy var onSelection = CKFMessage.Passthrough<Int>()
    
    public init(options: [String],
                font: UIFont,
                color: UIColor,
                tintColor: UIColor? = nil) {
        
        super.init(frame: .zero)
        initialize()
        setOptions(options, font: font, color: color, tintColor: tintColor)
    }
    
    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        initialize()
    }
    
    public func setOptions(_ options: [String], font: UIFont, color: UIColor, tintColor: UIColor?) {
        
        for (index, option) in options.enumerated() {
            self.insertSegment(withTitle: option, at: index, animated: false)
        }
        
        let attributes = [NSAttributedString.Key.font: font,
                        NSAttributedString.Key.foregroundColor: color]
        
        setTitleTextAttributes(attributes, for: .normal)
        
        if let tintColor = tintColor {
            let attributes = [NSAttributedString.Key.font: font,
                            NSAttributedString.Key.foregroundColor: tintColor]
            setTitleTextAttributes(attributes, for: .selected)
        }
    }
    
    private func initialize() {
        addTarget(self, action: #selector(selectionAction(sender:)), for: .valueChanged)
    }
    
    @objc private func selectionAction(sender: CKCSegmentedControl) {
        onSelection.callback?(sender.selectedSegmentIndex)
    }
    
    public func onSelection<Binder: AnyObject>(binder: Binder, _ callback: @escaping (Binder, Int) -> Void) {
        onSelection.observe(binder, with: callback)
    }
    
}
