//
//  CKCTextField.swift
//  CKCComponents
//
//  Created by Diego on 7/28/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit
import CKFoundation

public class CKCTextField: UITextField {

    internal static var lineColor = UIColor(white: 0.9, alpha: 1)
    
    private lazy var onChange = CKFMessage.Passthrough<CKCTextField>()
    private lazy var onBeginEdit = CKFMessage.Passthrough<CKCTextField>()
    private lazy var onEndEdit = CKFMessage.Passthrough<CKCTextField>()
    private lazy var onReturnTap = CKFMessage.Passthrough<CKCTextField>()
    private lazy var onClear = CKFMessage.Passthrough<CKCTextField>()
    
    public init(text: String?,
         font: UIFont? = nil,
         color: UIColor? = nil,
         tint: UIColor? = nil) {
        super.init(frame: .zero)
        self.text = text
        initialize()
        setText(font: font, color: color, tint: tint)
    }
    
    public init(attributedText: NSAttributedString) {
        super.init(frame: .zero)
        self.attributedText = attributedText
        initialize()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    public func setText(font: UIFont?, color: UIColor?, tint: UIColor?) {
        if let font = font {
            self.font = font
        }
        if let color = color {
            self.textColor = color
        }
        if let tint = tint {
            self.tintColor = tint
        }
    }
    
    public func setPlaceholder(_ placeholder: String?, font: UIFont?, color: UIColor?) {
        
        var attributes: [NSAttributedString.Key: Any] = [:]
        
        if let font = font {
            attributes[NSAttributedString.Key.font] = font
        }
        
        if let color = color {
            attributes[NSAttributedString.Key.foregroundColor] = color
        }
        
        if let text = text, !attributes.isEmpty {
            self.attributedPlaceholder = NSAttributedString(string: text, attributes: attributes)
        } else {
            self.placeholder = text
        }
        
    }
    
    public func initialize() {
        delegate = self
        addTarget(self, action: #selector(didChangeText(textField:)), for: .editingChanged)
    }
    
}

extension CKCTextField {
    
    public func onClear<Binder: AnyObject>(binder: Binder, _ callback: @escaping (Binder, CKCTextField) -> Void) {
        onClear.observe(binder, with: callback)
    }
    
    public func onChange<Binder: AnyObject>(binder: Binder, _ callback: @escaping (Binder, CKCTextField) -> Void) {
        onChange.observe(binder, with: callback)
    }
    
    public func onBeginEdit<Binder: AnyObject>(binder: Binder, _ callback: @escaping (Binder, CKCTextField) -> Void) {
        onBeginEdit.observe(binder, with: callback)
    }
    
    public func onEndEdit<Binder: AnyObject>(binder: Binder, _ callback: @escaping (Binder, CKCTextField) -> Void) {
        onEndEdit.observe(binder, with: callback)
    }
    
    public func onReturn<Binder: AnyObject>(binder: Binder, _ callback: @escaping (Binder, CKCTextField) -> Void) {
        onReturnTap.observe(binder, with: callback)
    }
    
}

extension CKCTextField: UITextFieldDelegate {
    
    @objc private func didChangeText(textField: UITextField) {
        onChange.callback?(self)
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        onClear.callback?(self)
        return true
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField) {
        onBeginEdit.callback?(self)
    }
    
    public func textFieldDidEndEditing(_ textField: UITextField) {
        onEndEdit.callback?(self)
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        onReturnTap.callback?(self)
        return true
    }
    
}
