//
//  CKCBorderedTextField.swift
//  CKCComponents
//
//  Created by Diego on 8/23/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

public class CKCBorderedTextField: CKCTextField {

    private var borderLayer: CALayer?
    private(set) var borderWidth: CGFloat = 1 {
        didSet {
            if oldValue != borderWidth {
                borderLayer?.borderWidth = borderWidth
            }
        }
    }
    private(set) var borderColor: UIColor = CKCTextField.lineColor {
        didSet {
            if oldValue != borderColor {
                borderLayer?.borderColor = borderColor.cgColor
            }
        }
    }
    private(set) var borderRadius: CGFloat = 5 {
        didSet {
            if oldValue != borderRadius {
                borderLayer?.cornerRadius = borderRadius
            }
        }
    }
    
    public override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        
        if borderLayer == nil {
            borderLayer = CALayer()
            borderLayer?.borderWidth = borderWidth
            borderLayer?.borderColor = borderColor.cgColor
            borderLayer?.cornerRadius = borderRadius
            borderLayer?.frame = bounds
            layer.addSublayer(borderLayer!)
        } else {
            borderLayer?.frame = bounds
        }
        
    }
    
    public func set(borderWidth: CGFloat = 1, color: UIColor, radius: CGFloat = 5) {
        self.borderWidth = borderWidth
        self.borderColor = color
        self.borderRadius = radius
    }
    
    override public func initialize() {
        super.initialize()
        borderStyle = .none
    }
    
    public override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 0)
    }
    
    public override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 10, dy: 0)
    }

}
