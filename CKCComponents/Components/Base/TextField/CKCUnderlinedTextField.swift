//
//  CKCUnderlinedTextField.swift
//  CKCComponents
//
//  Created by Diego on 8/23/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

public class CKCUnderlinedTextField: CKCTextField {

    private var lineShapeLayer: CAShapeLayer?
    
    private(set) var pathWidth: CGFloat = 1 {
        didSet {
            if oldValue != pathWidth {
                lineShapeLayer?.lineWidth = pathWidth
                lineShapeLayer?.path = drawUnderLine(inRect: bounds, lineWidth: pathWidth).cgPath
            }
        }
    }
    private(set) var pathColor: UIColor = CKCTextField.lineColor  {
        didSet {
            if oldValue != pathColor {
                lineShapeLayer?.strokeColor = pathColor.cgColor
            }
        }
    }
    
    override public func initialize() {
        super.initialize()
        borderStyle = .none
    }
    
    public override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        
        if lineShapeLayer == nil {
            lineShapeLayer = CAShapeLayer()
            lineShapeLayer?.path = drawUnderLine(inRect: bounds, lineWidth: pathWidth).cgPath
            lineShapeLayer?.strokeColor = pathColor.cgColor
            lineShapeLayer?.lineWidth = pathWidth
            lineShapeLayer?.frame = bounds
            layer.addSublayer(lineShapeLayer!)
        } else {
            CATransaction.begin()
            CATransaction.setDisableActions(true)
            
            lineShapeLayer?.frame = bounds
            lineShapeLayer?.path = drawUnderLine(inRect: bounds, lineWidth: pathWidth).cgPath
            
            CATransaction.commit()

        }
    }
    
    public func set(lineWidth: CGFloat = 1, color: UIColor) {
        pathWidth = lineWidth
        pathColor = color
    }
    
    private func drawUnderLine(inRect rect: CGRect, lineWidth: CGFloat) -> UIBezierPath {
        let standardPath = UIBezierPath()
        standardPath.move(to: CGPoint(x: 0, y: rect.height - (lineWidth * 0.5)))
        standardPath.addLine(to: CGPoint(x: rect.width, y: rect.height - (lineWidth * 0.5)))
        standardPath.close()
        return standardPath
    }

}
