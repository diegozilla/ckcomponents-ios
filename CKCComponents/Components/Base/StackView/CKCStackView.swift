//
//  CKCStackView.swift
//  CKCComponents
//
//  Created by Diego on 9/6/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

public class CKCStackView: UIStackView {

    public convenience init(_ subviews: [UIView],
                            spacing: CGFloat = 5,
                            axis: NSLayoutConstraint.Axis = .vertical,
                            distribution: UIStackView.Distribution = .fillEqually,
                            alignment: UIStackView.Alignment = .fill) {
        self.init(arrangedSubviews: subviews)
        self.spacing = spacing
        self.axis = axis
        self.distribution = distribution
        self.alignment = alignment
    }
    
    public func addSubviews(_ views: [UIView]) {
        views.forEach { self.addArrangedSubview($0) }
    }

}
