//
//  CKCArrangedScrollView.swift
//  CKCComponents
//
//  Created by Diego on 9/6/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

public class CKCStackedScrollView: UIScrollView {

    private var heightConstraint: NSLayoutConstraint!
    private var widthConstraint: NSLayoutConstraint!
    
    public var scrollAxis: NSLayoutConstraint.Axis {
        set {
            stackView.axis = newValue
            switch newValue {
            case .horizontal:
                widthConstraint.isActive = false
                heightConstraint.isActive = true
                layoutIfNeeded()
            case .vertical:
                heightConstraint.isActive = false
                widthConstraint.isActive = true
                layoutIfNeeded()
            @unknown default:
                break
            }
        }
        get {
            return stackView.axis
        }
    }
    
    private lazy var stackView: CKCStackView = {
        let stackView = CKCStackView([])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    public init(_ subviews: [UIView]) {
        super.init(frame: .zero)
        setStackView()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setStackView()
    }
    
    private func setStackView() {
        
        addSubview(stackView)
        
        widthConstraint = stackView.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1)
        heightConstraint = stackView.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 1)
        
        NSLayoutConstraint.activate([
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor),
            widthConstraint
        ])
    }
    
    public func addSubViews(_ subviews: [UIView]) {
        stackView.addSubviews(subviews)
    }

}
