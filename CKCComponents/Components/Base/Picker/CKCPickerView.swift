//
//  CKCPickerView.swift
//  CKCComponents
//
//  Created by Diego on 9/8/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit
import CKFoundation

public class CKCPlainPickerView: UIPickerView {

    public typealias PickerOutput = (column: Int, index: Int, value: String)
    
    fileprivate(set) var items: [[String]] = []
    
    fileprivate var pickerFont: UIFont?
    fileprivate var pickerTextColor: UIColor?
    fileprivate var pickerItemHeight: CGFloat = 40
    
    fileprivate var onSelection: CKFMessage.Passthrough<PickerOutput>?
    
    public init(column: [String],
                font: UIFont? = nil,
                textColor: UIColor? = nil,
                height: CGFloat = 40) {
        super.init(frame: .zero)
        self.setItems([column])
        self.pickerFont = font
        self.pickerTextColor = textColor
        self.pickerItemHeight = height
    }
    
    public init(columns: [[String]],
                font: UIFont? = nil,
                textColor: UIColor? = nil,
                height: CGFloat = 40) {
        super.init(frame: .zero)
        self.setItems(columns)
        self.pickerFont = font
        self.pickerTextColor = textColor
        self.pickerItemHeight = height
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func setItems(_ items: [[String]]) {
        self.items = items
        self.dataSource = self
        self.delegate = self
    }
    
    public func onSelection<Binder: AnyObject>(binder: Binder, _ callback: @escaping (Binder, PickerOutput) -> Void) {
        onSelection = CKFMessage.Passthrough()
        onSelection?.observe(binder, with: callback)
    }
    
}

extension CKCPlainPickerView: UIPickerViewDelegate {
    
    public func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return pickerItemHeight
    }
    
    public func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if let _ = self.pickerFont, let _ = self.pickerTextColor {
            return nil
        }
        return self.items[component][row]
    }
    
    public func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        guard let font = self.pickerFont, let color = self.pickerTextColor else {
            return nil
        }
        return NSAttributedString(string: self.items[component][row], attributes: [NSAttributedString.Key.font : font,
                                                                                   NSAttributedString.Key.foregroundColor: color])
    }
    
    public func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let value = self.items[component][row]
        onSelection?.callback?((column: component, index: row, value: value))
    }
    
}

extension CKCPlainPickerView: UIPickerViewDataSource {
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return items[component].count
    }
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return items.count
    }
    
}
