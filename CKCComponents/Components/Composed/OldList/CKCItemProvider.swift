//
//  CKCItemProvider.swift
//  CKCComponents
//
//  Created by Diego on 7/27/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

public class CKCListItemProvider<Section, Item> {
    
    public typealias ItemInput = (collectionView: UICollectionView, indexPath: IndexPath, item: Item)
    public typealias SectionInput = (collectionView: UICollectionView, indexPath: IndexPath, section: Section)

    internal var onItem: CKCMessage.PassthroughOutput<ItemInput, UICollectionViewCell>?
    internal var onSection: CKCMessage.PassthroughOutput<SectionInput, UICollectionReusableView>?
    
    deinit {
        print("Deinitializing CKCListItemProvider")
    }
    
    public init() {}
    
    internal func onItem<Binder: AnyObject>(binder: Binder, _ itemProvider: @escaping (Binder, ItemInput) -> UICollectionViewCell) {
        onItem = CKCMessage.PassthroughOutput()
        onItem?.observe(binder, with: itemProvider)
    }
    
    internal func onSection<Binder: AnyObject>(binder: Binder, _ sectionProvider: @escaping (Binder, SectionInput) -> UICollectionReusableView) {
        onSection = CKCMessage.PassthroughOutput()
        onSection?.observe(binder, with: sectionProvider)
    }
}
