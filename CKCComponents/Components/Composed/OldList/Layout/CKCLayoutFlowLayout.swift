//
//  CKCLayoutFlowLayout.swift
//  CKCComponents
//
//  Created by Diego on 7/21/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

extension UIEdgeInsets {
    public static var CKCBaseFlowLayoutMargin = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
}

public class CKCCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    public var itemHeight: CGFloat = 0
    public var itemWidth: CGFloat = 0
    
    public var itemsPerLine = 1
    
    public init(itemsPerRow: Int = 1,
         height: CGFloat = 40,
         margin: UIEdgeInsets = UIEdgeInsets.CKCBaseFlowLayoutMargin,//(top: 10, left: 15, bottom: 10, right: 15),
         innerSpacing: CGFloat = 5,
         rowSpacing: CGFloat = 10) {
        
        self.itemHeight = height
        self.itemsPerLine = itemsPerRow
        
        super.init()
        
        sectionInset = margin
        minimumInteritemSpacing = innerSpacing
        minimumLineSpacing = rowSpacing
        scrollDirection = .vertical
    }
    
    public init(itemsPerColumn: Int = 1,
         width: CGFloat = 40,
         margin: UIEdgeInsets = UIEdgeInsets.CKCBaseFlowLayoutMargin,//(top: 10, left: 15, bottom: 10, right: 15),
         innerSpacing: CGFloat = 5,
         columnSpacing: CGFloat = 10) {
        
        self.itemWidth = width
        self.itemsPerLine = itemsPerColumn
        
        super.init()
        
        sectionInset = margin
        minimumInteritemSpacing = innerSpacing
        minimumLineSpacing = columnSpacing
        scrollDirection = .horizontal
    }
    
    override public func prepare() {
        super.prepare()
        
        guard let collectionView = collectionView else { return }
        
        switch scrollDirection {
        case .vertical:
            let leftMargin = sectionInset.left
            let rightMargin = sectionInset.right
            
            let itemSpacingWidth = minimumInteritemSpacing * CGFloat(itemsPerLine - 1)
            
            let availableWidth = collectionView.bounds.width - leftMargin - rightMargin - itemSpacingWidth
            itemWidth = (availableWidth / CGFloat(itemsPerLine)).rounded(.down)

        case .horizontal:
            
            let topMargin = sectionInset.top
            let bottomMargin = sectionInset.bottom
            
            let itemSpacingWidth = minimumInteritemSpacing * CGFloat(itemsPerLine - 1)
            
            let availableHeight = collectionView.bounds.height - topMargin - bottomMargin - itemSpacingWidth
            itemHeight = (availableHeight / CGFloat(itemsPerLine)).rounded(.down)
            
        @unknown default:
            fatalError("Needs support for new direction")
        }

        itemSize = CGSize(width: itemWidth, height: itemHeight)
        self.sectionInsetReference = .fromSafeArea
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}

extension CKCCollectionViewFlowLayout {
    
    public static func rowLayout(height: CGFloat,
                                    margin: UIEdgeInsets,
                                    rowSpacing: CGFloat = 10) -> CKCCollectionViewFlowLayout {
        
        return CKCCollectionViewFlowLayout(itemsPerRow: 1,
                                           height: height,
                                           margin: margin,
                                           innerSpacing: 0,
                                           rowSpacing: rowSpacing)
    }
    
    public static func columnLayout(width: CGFloat,
                                 margin: UIEdgeInsets,
                                 columnSpacing: CGFloat = 10) -> CKCCollectionViewFlowLayout {
        
        return CKCCollectionViewFlowLayout(itemsPerColumn: 1,
                                           width: width,
                                           margin: margin,
                                           innerSpacing: 0,
                                           columnSpacing: columnSpacing)
        
    }
    
}
