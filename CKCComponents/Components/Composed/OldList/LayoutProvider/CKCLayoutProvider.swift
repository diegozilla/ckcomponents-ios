//
//  CKCLayoutProvider.swift
//  CKCComponents
//
//  Created by Diego on 7/27/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

public protocol CKCListLayoutProvider {
    var layout: UICollectionViewLayout { get }
}
