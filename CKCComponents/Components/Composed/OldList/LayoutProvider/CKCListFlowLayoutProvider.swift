//
//  CKCListFlowLayoutProvider.swift
//  CKCComponents
//
//  Created by Diego on 9/1/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

public class CKCListFlowLayoutProvider: CKCListLayoutProvider {
    
    deinit {
        print("Deinitializing CKCListFlowLayoutProvider")
    }
    
    public var layout: UICollectionViewLayout {
        return flowLayout
    }
    
    public let flowLayout: CKCCollectionViewFlowLayout
    
    public init(rowHeight: CGFloat,
                margin: UIEdgeInsets,
                spacing: CGFloat) {
        flowLayout = CKCCollectionViewFlowLayout.rowLayout(height: rowHeight, margin: margin, rowSpacing: spacing)
    }
    
    public init(columnWidth: CGFloat,
                margin: UIEdgeInsets,
                spacing: CGFloat) {
        flowLayout = CKCCollectionViewFlowLayout.columnLayout(width: columnWidth, margin: margin, columnSpacing: spacing)
    }
    
    public init(itemsPerRow: Int,
                rowHeight: CGFloat,
                margin: UIEdgeInsets,
                innerSpacing: CGFloat,
                spacing: CGFloat) {
        flowLayout = CKCCollectionViewFlowLayout(itemsPerRow: itemsPerRow, height: rowHeight, margin: margin, innerSpacing: innerSpacing, rowSpacing: spacing)
    }
    
    public init(itemsPerColumn: Int,
                columnWidth: CGFloat,
                margin: UIEdgeInsets,
                innerSpacing: CGFloat,
                spacing: CGFloat) {
        flowLayout = CKCCollectionViewFlowLayout(itemsPerColumn: itemsPerColumn, width: columnWidth, margin: margin, innerSpacing: innerSpacing, columnSpacing: spacing)
    }
    
}
