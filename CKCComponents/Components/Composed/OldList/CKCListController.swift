//
//  CKCListView.swift
//  CKCComponents
//
//  Created by Diego on 7/27/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

public class CKCListController<Section, Item> {
    
    private(set) var collectionView: UICollectionView
    private var listState: CKCListState<Section, Item>
    private var layoutProvider: CKCListLayoutProvider
    private var itemProvider: CKCListItemProvider<Section, Item>
    
    public var currentState: CKCListState<Section, Item> { return listState }
    
    deinit {
        print("Deinitializing CKCListView")
    }
    
    public init(collectionView: UICollectionView,
                data: [CKCListSection<Section, Item>],
                layoutProvider: CKCListLayoutProvider,
                delegate: UICollectionViewDelegate? = nil) {
        
        self.collectionView = collectionView
        self.layoutProvider = layoutProvider
        self.itemProvider = CKCListItemProvider<Section, Item>()
        self.listState = CKCListState(data: data)
        
        self.collectionView.delegate = delegate
        self.collectionView.collectionViewLayout = self.layoutProvider.layout
        self.collectionView.dataSource = self.listState.dataSource
        
        self.listState.collectionView = self.collectionView
        self.listState.itemProvider = self.itemProvider

    }

    public func onItem<Binder: AnyObject>(binder: Binder,
                                          _ itemProvider: @escaping (Binder, CKCListItemProvider<Section, Item>.ItemInput) -> UICollectionViewCell) {
        self.itemProvider.onItem(binder: binder, itemProvider)
    }
    
    public func onSection<Binder: AnyObject>(binder: Binder,
                                             _ sectionProvider: @escaping (Binder, CKCListItemProvider<Section, Item>.SectionInput) -> UICollectionReusableView) {
        self.itemProvider.onSection(binder: binder, sectionProvider)
    }

}

