//
//  CKCListController+Initializers.swift
//  CKCComponents
//
//  Created by Diego on 8/31/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

extension CKCListController {
    
    public static func createVerticalList(from collectionView: UICollectionView,
                                  withData data: [CKCListSection<Section, Item>],
                                  rowHeight: CGFloat,
                                  margin: UIEdgeInsets,
                                  spacing: CGFloat,
                                  delegate: UICollectionViewDelegate? = nil) -> CKCListController {
        
        let flowLayout = CKCListFlowLayoutProvider(rowHeight: rowHeight, margin: margin, spacing: spacing)
        
        
        return CKCListController(collectionView: collectionView,
                                 data: data,
                                 layoutProvider: flowLayout,
                                 delegate: delegate)
        
    }
    
    public static func createVerticalList(from collectionView: UICollectionView,
                                          withItems items: [Item],
                                          rowHeight: CGFloat,
                                          margin: UIEdgeInsets,
                                          spacing: CGFloat,
                                          delegate: UICollectionViewDelegate? = nil) -> CKCListController {
        
        let flowLayout = CKCListFlowLayoutProvider(rowHeight: rowHeight, margin: margin, spacing: spacing)
        let data = CKCListSection<Section, Item>(items: items)
        return CKCListController(collectionView: collectionView, data: [data], layoutProvider: flowLayout)
    }
    
    public static func createHorizontalList(from collectionView: UICollectionView,
                                          withData data: [CKCListSection<Section, Item>],
                                          columnWidth: CGFloat,
                                          margin: UIEdgeInsets,
                                          spacing: CGFloat,
                                          delegate: UICollectionViewDelegate? = nil) -> CKCListController {
        
        let flowLayout = CKCListFlowLayoutProvider(columnWidth: columnWidth, margin: margin, spacing: spacing)
        
        
        return CKCListController(collectionView: collectionView,
                                 data: data,
                                 layoutProvider: flowLayout,
                                 delegate: delegate)
        
    }
    
    public static func createHorizontalList(from collectionView: UICollectionView,
                                          withItems items: [Item],
                                          columnWidth: CGFloat,
                                          margin: UIEdgeInsets,
                                          spacing: CGFloat,
                                          delegate: UICollectionViewDelegate? = nil) -> CKCListController {
        
         let flowLayout = CKCListFlowLayoutProvider(columnWidth: columnWidth, margin: margin, spacing: spacing)
        let data = CKCListSection<Section, Item>(items: items)
        return CKCListController(collectionView: collectionView, data: [data], layoutProvider: flowLayout)
    }
}
