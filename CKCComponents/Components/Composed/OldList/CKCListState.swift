//
//  CKCListBinding.swift
//  CKCComponents
//
//  Created by Diego on 7/27/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

public struct CKCListSection<Section, Item> {
    public var section: Section?
    public var items: [Item]
    
    public init(section: Section? = nil, items: [Item]) {
        self.section = section
        self.items = items
    }
}

public class CKCListState<Section, Item> {

    deinit {
        print("Deinitializing CKCListState")
    }

    fileprivate var _dataSource: DataSource
    private(set) var isExecutingInBatch = false
    private var batchActionQueue: [BatchAction]?
    
    
    public var data: [CKCListSection<Section, Item>] {
        return _dataSource.data
    }
    
    public var items: [[Item]] {
        return _dataSource.data.map { $0.items }
    }
    
    public var sections: [Section?] {
        return _dataSource.data.map { $0.section }
    }
    
    internal weak var collectionView: UICollectionView?
    internal var dataSource: UICollectionViewDataSource { return _dataSource }
    
    public weak var itemProvider: CKCListItemProvider<Section, Item>? {
        didSet { _dataSource.itemProvider = itemProvider }
    }
    
    public init(data: [CKCListSection<Section, Item>]) {
        self._dataSource = DataSource(data: data)
    }
    
    private func adjustIndex(_ index: Int) -> Int {
        if index < 0 { return 0 }
        else if index >= _dataSource.data.count { return _dataSource.data.count }
        else { return index }
    }
    
    private func isIndexInRange(_ index: Int) -> Bool {
        return index >= 0 && index < _dataSource.data.count
    }
    
    public func add(listSection: CKCListSection<Section, Item>, atIndex index: Int, withRefresh refresh: Bool = true) {
        guard isIndexInRange(index) else { return }
        self._dataSource.add(listSection, atIndex: index)
        if refresh { collectionView?.insertSections(IndexSet(integer: index)) }
    }
    
    public func update(section listSection: CKCListSection<Section, Item>, atIndex index: Int, withRefresh refresh: Bool = true) {
        guard isIndexInRange(index) else { return }
        self._dataSource.update(listSection, atIndex: index)
        if refresh { collectionView?.reloadSections(IndexSet(integer: index)) }
    }
    
    public func update(section: Section?, atIndex index: Int, withRefresh refresh: Bool = true) {
        guard isIndexInRange(index) else { return }
        self._dataSource.update(section: section, atIndex: index)
        if refresh { collectionView?.reloadSections(IndexSet(integer: index)) }
    }
    
    public func delete(sectionAtIndex index: Int, withRefresh refresh: Bool = true) {
        guard isIndexInRange(index) else { return }
        self._dataSource.remove(atIndex: index)
        if refresh { collectionView?.deleteSections(IndexSet(integer: index)) }
    }
    
    public func insert(item: Item, indexPath: IndexPath, withRefresh refresh: Bool = true) {
        _dataSource.data[indexPath.section].items.insert(item, at: indexPath.item)
        guard !isExecutingInBatch else {
            batchActionQueue?.append(BatchAction(type: .insert, indexPath: indexPath))
            return
        }
        if refresh { collectionView?.insertItems(at: [indexPath]) }
    }
    
    public func update(item: Item, indexPath: IndexPath, withRefresh refresh: Bool = true) {
        _dataSource.data[indexPath.section].items[indexPath.item] = item
        guard !isExecutingInBatch else {
            batchActionQueue?.append(BatchAction(type: .update, indexPath: indexPath))
            return
        }
        if refresh { collectionView?.reloadItems(at: [indexPath]) }
    }
    
    public func delete(indexPath: IndexPath, withRefresh refresh: Bool = true) {
        _dataSource.data[indexPath.section].items.remove(at: indexPath.item)
        guard !isExecutingInBatch else {
            batchActionQueue?.append(BatchAction(type: .delete, indexPath: indexPath))
            return
        }
        if refresh { collectionView?.deleteItems(at: [indexPath]) }
    }
    
    public func reload(data: [CKCListSection<Section, Item>]) {
        _dataSource.data = data
        collectionView?.reloadData()
    }
    
    public func prepareForBatchExecution() {
        isExecutingInBatch = true
        batchActionQueue = []
    }
    
    public func executeBatch() {
        
        collectionView?.performBatchUpdates({
            
            guard let queue = self.batchActionQueue else { return }
            
            queue.forEach { batchAction in
                switch batchAction.type {
                case .delete:
                    self.collectionView?.deleteItems(at: [batchAction.indexPath])
                case .insert:
                    self.collectionView?.insertItems(at: [batchAction.indexPath])
                case .update:
                    self.collectionView?.reloadItems(at: [batchAction.indexPath])
                }
            }
            
        }, completion: { _ in
            self.batchActionQueue = nil
            self.isExecutingInBatch = false
        })
    }
}

extension CKCListState {
    
    fileprivate struct BatchAction {
        fileprivate var type: BatchUpdateType
        fileprivate var indexPath: IndexPath
        
        fileprivate init(type: BatchUpdateType, indexPath: IndexPath) {
            self.type = type
            self.indexPath = indexPath
        }
    }
    
    fileprivate enum BatchUpdateType {
        case insert, update, delete
    }
    
}

extension CKCListState {
    
    fileprivate class DataSource: NSObject, UICollectionViewDataSource {
        
        fileprivate var data: [CKCListSection<Section, Item>]
        fileprivate weak var itemProvider: CKCListItemProvider<Section, Item>?
        
        fileprivate init(data: [CKCListSection<Section, Item>]) {
            self.data = data
        }
        
        fileprivate func add(_ listSection: CKCListSection<Section, Item>, atIndex index: Int) {
            self.data.insert(listSection, at: index)
        }
        
        fileprivate func remove(atIndex index: Int) {
            self.data.remove(at: index)
        }
        
        fileprivate func update(_ listSection: CKCListSection<Section, Item>, atIndex index: Int) {
            self.data[index] = listSection
        }
        
        fileprivate func update(section: Section?, atIndex index: Int) {
            self.data[index].section = section
        }
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            return data.count
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return data[section].items.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let currentItem = data[indexPath.section].items[indexPath.item]
            guard let cell = itemProvider?.onItem?.callback?((collectionView, indexPath, currentItem)) else { return UICollectionViewCell() }
            return cell
        }
        
    }
    
}
