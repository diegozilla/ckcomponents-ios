//
//  CKCListController.swift
//  CollectionViewTest
//
//  Created by Diego on 9/9/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

public class CKCListController<Section, Item> where Section: CKCListSection, Item: CKCListItem {
    
    private var collectionView: UICollectionView
    private(set) var dataSource: CKCListDataSource<Section, Item>
    
    public init(section: Section,
                items: [Item],
                collectionView: UICollectionView,
                itemProvider: @escaping CKCListItemProvider<Item>) {
        
        self.collectionView = collectionView
        let data = CKCListContent(section: section, items: items)
        self.dataSource = CKCListDataSource(data: [data],
                                collectionView: collectionView,
                                itemProvider: itemProvider)
    }
    
    public init(data: [CKCListContent<Section, Item>],
                collectionView: UICollectionView,
                itemProvider: @escaping CKCListItemProvider<Item>) {
        
        self.collectionView = collectionView
        self.dataSource = CKCListDataSource(data: data,
                                collectionView: collectionView,
                                itemProvider: itemProvider)
    }
    
    public func setSectionProvider(_ sectionProvider: @escaping CKCListSectionProvider<Section>) {
        self.dataSource.provideSection(sectionProvider)
    }
    
}
