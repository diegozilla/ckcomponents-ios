//
//  CKCListRegularDataSource.swift
//  CollectionViewTest
//
//  Created by Diego on 9/9/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

class CKCListBaseDataSource<Section, Item>: CKCListDataSourceProtocol where Section: CKCListSection, Item: CKCListItem {    
    
    var dataSource: DataSource
    
    weak var collectionView: UICollectionView?
    
    init(data: [CKCListContent<Section, Item>],
        collectionView: UICollectionView,
         itemProvider: @escaping CKCListItemProvider<Item>) {

        self.dataSource = DataSource(data: data, itemProvider: itemProvider)
        self.collectionView = collectionView
        self.collectionView?.dataSource = dataSource
    }
    
    func getData() -> [CKCListContent<Section, Item>]{
        return dataSource.data
    }
    
    private func isIndexInRange(_ index: Int, ofSection sectionIndex: Int) -> Bool {
        return index >= 0 && index < dataSource.data[sectionIndex].items.count
    }
    
    private func isSectionIndexInRange(_ index: Int) -> Bool {
        return index >= 0 && index <= dataSource.data.count
    }
    
    private func sectionIndex(_ section: Section) -> Int? {
        return dataSource.data.firstIndex(where: { $0.section == section })
    }
    
    func append(section: Section, items: [Item]) {
        insert(section: section, items: items, atIndex: dataSource.data.count)
    }
    
    func appendItems(items: [Item], into section: Section) {
        guard let sectionIndex = sectionIndex(section) else { return }
        insertItems(items: items, into: section, at: dataSource.data[sectionIndex].items.count)
    }
    
    func insert(section: Section, items: [Item], atIndex index: Int) {
        
        guard isSectionIndexInRange(index) else { return }

        let container = CKCListContent(section: section, items: items)
        dataSource.insert(container, atIndex: index)
        let sectionIndexSet = IndexSet(integer: index)
        
        collectionView?.insertSections(sectionIndexSet)
    }
    
    func insertItems(items: [Item], into section: Section, at index: Int) {
        
        guard let sectionIndex = sectionIndex(section) else { return }
        var itemsIndexPaths: [IndexPath] = []
        
        for (itemIndex, _) in items.enumerated() {
            itemsIndexPaths.append(IndexPath(item: index + itemIndex, section: sectionIndex))
        }
        
        dataSource.data[sectionIndex].items.insert(contentsOf: items, at: index)
        collectionView?.insertItems(at: itemsIndexPaths)
    }
    
    func update(item: Item, withNewItem newItem: Item, fromSection section: Section) {
        
        guard let sectionIndex = sectionIndex(section) else { return }
        guard let itemIndex = self.dataSource.data[sectionIndex].items.firstIndex(where: { $0 == item }) else { return }
        dataSource.data[sectionIndex].items[itemIndex] = newItem
        collectionView?.reloadItems(at: [IndexPath(item: itemIndex, section: sectionIndex)])
        
    }
    
    func delete(section: Section) {
        guard let sectionIndex = sectionIndex(section) else { return }
        dataSource.remove(sectionIndex: sectionIndex)
        let sectionIndexSet = IndexSet(integer: sectionIndex)
        
        collectionView?.deleteSections(sectionIndexSet)
    }
    
    func deleteItems(items: [Item], from section: Section) {
        guard let sectionIndex = sectionIndex(section) else { return }
        var itemsIndexPaths: [IndexPath] = []
        var itemsCopy: [Item] = []
        
        for item in items {
            if let itemIndex = self.dataSource.data[sectionIndex].items.firstIndex(where: { $0 == item }) {
                itemsCopy.append(item)
                itemsIndexPaths.append(IndexPath(item: itemIndex, section: sectionIndex))
            }
        }
        
        dataSource.remove(itemsCopy, atSectionIndex: sectionIndex)
        collectionView?.deleteItems(at: itemsIndexPaths)
    }
    
    func provideSection(_ sectionProvider: @escaping CKCListSectionProvider<Section>) {
        dataSource.sectionProvider = sectionProvider
    }
}

extension CKCListBaseDataSource {

    func prepareForBatchExecution() {
    }
    
    func executeBatch() {
    }
}

extension CKCListBaseDataSource {
    
    class DataSource: NSObject, UICollectionViewDataSource {
        
        fileprivate var data: [CKCListContent<Section, Item>]
        fileprivate var itemProvider: CKCListItemProvider<Item>
        fileprivate var sectionProvider: CKCListSectionProvider<Section>?
        
        fileprivate init(data: [CKCListContent<Section, Item>],
                         itemProvider: @escaping CKCListItemProvider<Item>) {
            self.data = data
            self.itemProvider = itemProvider
            super.init()
        }
        
        fileprivate func append(_ listSection: CKCListContent<Section, Item>) {
            self.data.append(listSection)
        }
        
        fileprivate func insert(_ listSection: CKCListContent<Section, Item>, atIndex index: Int) {
            self.data.insert(listSection, at: index)
        }
        
        fileprivate func remove(sectionIndex: Int) {
            self.data.remove(at: sectionIndex)
        }
        
        fileprivate func remove(_ items: [Item], atSectionIndex index: Int) {
            items.forEach { item in
                self.data[index].items.removeAll(where: { $0 == item })
            }
        }
        
        fileprivate func update(_ listSection: CKCListContent<Section, Item>, atIndex index: Int) {
            self.data[index] = listSection
        }
        
        fileprivate func update(section: Section, atIndex index: Int) {
            self.data[index].section = section
        }
        
        func numberOfSections(in collectionView: UICollectionView) -> Int {
            return data.count
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return data[section].items.count
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let currentItem = data[indexPath.section].items[indexPath.item]
            guard let cell = itemProvider(collectionView, indexPath, currentItem) else {
                return UICollectionViewCell()
            }
            return cell
        }
        
        func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
            guard let supplementaryView = sectionProvider?(collectionView, kind, indexPath, data[indexPath.section].section) else { return UICollectionReusableView() }
            return supplementaryView
        }
        
    }
    
}
