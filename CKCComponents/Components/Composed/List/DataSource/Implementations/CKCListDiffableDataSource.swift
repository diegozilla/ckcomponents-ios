//
//  CKCListDiffableDataSource.swift
//  CollectionViewTest
//
//  Created by Diego on 9/9/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

@available(iOS 13.0, *)

class CKCListDiffableDataSource<Section, Item>: CKCListDataSourceProtocol where Section: CKCListSection, Item: CKCListItem {
    
    typealias ListSection = Section
    typealias ListItem = Item
    
    var dataSource: UICollectionViewDiffableDataSource<Section, Item>
    var _data: [CKCListContent<Section, Item>] = []
    
    init(data: [CKCListContent<Section, Item>],
         collectionView: UICollectionView,
         cellProvider: @escaping CKCListItemProvider<Item>) {
        self._data = data
        dataSource = UICollectionViewDiffableDataSource(collectionView: collectionView, cellProvider: cellProvider)
        
        var snapshot = dataSource.snapshot()
        
        for content in data {
            
            snapshot.appendSections([content.section])
            snapshot.appendItems(content.items, toSection: content.section)
        }
        

        dataSource.apply(snapshot, animatingDifferences: false, completion: nil)
    }
    
    private func isIndexInSectionRange(_ index: Int) -> Bool {
        return index >= 0 && index < _data.count
    }
    
    func getData() -> [CKCListContent<Section, Item>] {
        return _data
    }
    
    func append(section: Section, items: [Item]) {
        let snapshot = dataSource.snapshot()
        self.insert(section: section, items: items, atIndex: snapshot.sectionIdentifiers.count - 1)
    }
    
    func appendItems(items: [Item], into section: Section) {
        let snapshot = dataSource.snapshot()
        self.insertItems(items: items, into: section, at: snapshot.itemIdentifiers(inSection: section).count - 1)
    }
    
    func insert(section: Section, items: [Item], atIndex index: Int) {
        guard isIndexInSectionRange(index) else { return }
        var snapshot = dataSource.snapshot()
        
        let container = CKCListContent(section: section, items: items)
        _data.insert(container, at: index + 1)
        
        snapshot.insertSections([section], afterSection: _data[index].section)
        snapshot.appendItems(items, toSection: section)
        
        dataSource.apply(snapshot)
    }
    
    func insertItems(items: [Item], into section: Section, at index: Int) {
        var snapshot = dataSource.snapshot()
        
        guard let sectionIndex = snapshot.indexOfSection(section) else { return }
        let currentSection = snapshot.sectionIdentifiers[sectionIndex]
        let currentItems = snapshot.itemIdentifiers(inSection: currentSection)
        let currentItem = currentItems[index]
        
        _data[sectionIndex].items.insert(contentsOf: items, at: index + 1)
        snapshot.insertItems(items, afterItem: currentItem)
        
        dataSource.apply(snapshot)
    }
    
    func update(item: Item, withNewItem newItem: Item, fromSection section: Section) {
        
        var snapshot = dataSource.snapshot()
        guard let sectionIndex = snapshot.indexOfSection(section) else { return }
        guard let itemIndex = snapshot.indexOfItem(item) else { return }
        
        _data[sectionIndex].items[itemIndex] = newItem
        
        snapshot.insertItems([newItem], afterItem: item)
        snapshot.deleteItems([item])
        dataSource.apply(snapshot)
    }
    
    func delete(section: Section) {
        var snapshot = dataSource.snapshot()
        
        guard let sectionIndex = snapshot.indexOfSection(section) else { return }
        _data.remove(at: sectionIndex)
        
        snapshot.deleteSections([section])
        dataSource.apply(snapshot)
        
    }
    
    func deleteItems(items: [Item], from section: Section) {
        var snapshot = dataSource.snapshot()
        guard let sectionIndex = snapshot.indexOfSection(section) else { return }
        
        for item in items {
            if let itemIndex = snapshot.indexOfItem(item) {
                _data[sectionIndex].items.remove(at: itemIndex)
            }
        }
        
        snapshot.deleteItems(items)
        dataSource.apply(snapshot)
    }
    
    func provideSection(_ sectionProvider: @escaping CKCListSectionProvider<Section>) {
        dataSource.supplementaryViewProvider = { [unowned self] (collectionView, kind, indexPath) in
            let section = self._data[indexPath.section].section
            return sectionProvider(collectionView, kind, indexPath, section)
        }
    }
}

@available(iOS 13.0, *)
extension CKCListDiffableDataSource {
    
    func prepareForBatchExecution() {
    }
    
    func executeBatch() {
    }
}
