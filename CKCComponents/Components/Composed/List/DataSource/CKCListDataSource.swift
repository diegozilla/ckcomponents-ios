//
//  CKCListDataSource.swift
//  CollectionViewTest
//
//  Created by Diego on 9/9/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

final public class CKCListDataSource<Section, Item> where Section: CKCListSection, Item: CKCListItem {
    
    private var dataSource: CKCAnyListDataSource<Section, Item>
    
    init(data: [CKCListContent<Section, Item>],
         collectionView: UICollectionView,
         itemProvider: @escaping CKCListItemProvider<Item>) {
        
        if #available(iOS 13.0, *) {
            let diffableDataSource = CKCListDiffableDataSource<Section, Item>(data: data,
                                                                              collectionView: collectionView,
                                                                              cellProvider: itemProvider)
            self.dataSource = CKCAnyListDataSource(diffableDataSource)
        } else {
            let regularDataSource = CKCListBaseDataSource<Section, Item>(data: data,
                                                                            collectionView: collectionView,
                                                                            itemProvider: itemProvider)
            self.dataSource = CKCAnyListDataSource(regularDataSource)
        }
    }
    
}

extension CKCListDataSource: CKCListDataSourceProtocol {
    
    public func insert(section: Section, items: [Item], atIndex index: Int) {
        self.dataSource.insert(section: section, items: items, atIndex: index)
    }
    
    public func insertItems(items: [Item], into section: Section, at index: Int) {
        self.dataSource.insertItems(items: items, into: section, at: index)
    }
    
    public func append(section: Section, items: [Item]) {
        self.dataSource.append(section: section, items: items)
    }
    
    public func appendItems(items: [Item], into section: Section) {
        self.dataSource.appendItems(items: items, into: section)
    }
    
    public func update(item: Item, withNewItem newItem: Item, fromSection section: Section) {
        self.dataSource.update(item: item, withNewItem: newItem, fromSection: section)
    }
    
    public func delete(section: Section) {
        self.dataSource.delete(section: section)
    }
    
    public func deleteItems(items: [Item], from section: Section) {
        self.dataSource.deleteItems(items: items, from: section)
    }
    
    func prepareForBatchExecution() {
        self.dataSource.prepareForBatchExecution()
    }
    
    func executeBatch() {
        self.dataSource.executeBatch()
    }
    
    public func getData() -> [CKCListContent<Section, Item>] {
        return self.dataSource.getData()
    }
    
    func provideSection(_ sectionProvider: @escaping CKCListSectionProvider<Section>) {
        self.dataSource.provideSection(sectionProvider)
    }

}

extension CKCListDataSource {
    
    struct CKCAnyListDataSource<Section, Item>: CKCListDataSourceProtocol where Section: CKCListSection, Item: CKCListItem {
        
        typealias DataSection = Section
        typealias DataItem = Item

        var _append: (Section, [Item]) -> ()
        var _insert: (Section, [Item], Int) -> ()
        var _insertItems: ([Item], Section, Int) -> ()
        var _appendItems: ([Item], Section) -> ()
        var _update: (Item, Item, Section) -> ()
        var _delete: (Section) -> ()
        var _deleteItems: ([Item], Section) -> ()
        var _prepareForBatchExecution: () -> ()
        var _executeBatch: () -> ()
        var _provideSection: (@escaping CKCListSectionProvider<Section>) -> ()
        var _getData: () -> [CKCListContent<Section, Item>]
        
        init<DataSource: CKCListDataSourceProtocol>(_ dataSource: DataSource) where DataSource.Section == Section, DataSource.Item == Item {
            self._append = dataSource.append
            self._insert = dataSource.insert
            self._insertItems = dataSource.insertItems
            self._appendItems = dataSource.appendItems
            self._update = dataSource.update
            self._delete = dataSource.delete
            self._deleteItems = dataSource.deleteItems
            self._executeBatch = dataSource.executeBatch
            self._prepareForBatchExecution = dataSource.prepareForBatchExecution
            self._provideSection = dataSource.provideSection
            self._getData = dataSource.getData
        }
        
        func append(section: Section, items: [Item]) {
            _append(section, items)
        }
        
        func appendItems(items: [Item], into section: Section) {
            _appendItems(items, section)
        }
        
        func insert(section: Section, items: [Item], atIndex index: Int) {
            _insert(section, items, index)
        }
        
        func insertItems(items: [Item], into section: Section, at index: Int) {
            _insertItems(items, section, index)
        }
        
        func delete(section: Section) {
            _delete(section)
        }
        
        func deleteItems(items: [Item], from section: Section) {
            _deleteItems(items, section)
        }
        
        func prepareForBatchExecution() {
            _prepareForBatchExecution()
        }
        
        func executeBatch() {
            _executeBatch()
        }
        
        func update(item: Item, withNewItem newItem: Item, fromSection section: Section) {
            _update(item, newItem, section)
        }
        
        func getData() -> [CKCListContent<Section, Item>] {
            return _getData()
        }
        
        func provideSection(_ sectionProvider: @escaping CKCListSectionProvider<Section>) {
            _provideSection(sectionProvider)
        }
        
    }
    
}


