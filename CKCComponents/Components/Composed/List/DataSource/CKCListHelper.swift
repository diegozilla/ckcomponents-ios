//
//  CKCAnyListDataSource.swift
//  CKCListWrapper
//
//  Created by Diego on 9/9/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

public protocol CKCListSection: Hashable { }
public protocol CKCListItem: Hashable { }

public typealias CKCListItemProvider<Item: Hashable> = (_ collectionView: UICollectionView, _ indexPath: IndexPath, _ item: Item) -> UICollectionViewCell?

public typealias CKCListSectionProvider<Section: Hashable> = (_ collectionView: UICollectionView, _ kind: String, _ indexPath: IndexPath, _ section: Section) -> UICollectionReusableView?

public struct CKCListContent<Section, Item> {
    public var section: Section
    public var items: [Item]
}
