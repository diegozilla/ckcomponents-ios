//
//  CKCListDataSourceProtocol.swift
//  CKCListWrapper
//
//  Created by Diego on 10/6/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

protocol CKCListDataSourceProtocol {
    
    associatedtype Section: CKCListSection
    associatedtype Item: CKCListItem
    
    func getData() -> [CKCListContent<Section, Item>]
    
    func insert(section: Section, items: [Item], atIndex index: Int)
    func insertItems(items: [Item], into section: Section, at index: Int)
    
    func append(section: Section, items: [Item])
    func appendItems(items: [Item], into section: Section)
    
    func delete(section: Section)
    func deleteItems(items: [Item], from section: Section)
    
    func update(item: Item, withNewItem newItem: Item, fromSection section: Section)
    
    func provideSection(_ sectionProvider: @escaping CKCListSectionProvider<Section>)
    
    func prepareForBatchExecution()
    func executeBatch()
}
