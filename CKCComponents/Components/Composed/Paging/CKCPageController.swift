//
//  CKCPageController.swift
//  CKCComponents
//
//  Created by Diego on 9/1/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit
import CKFoundation

public class CKCPageViewController: UIViewController {
    
    private var pageViewController: UIPageViewController
    
    private var contentViewControllers: [UIViewController]?
    
    public var hasContinuousScroll = false
    private lazy var onSwitch = CKFMessage.Passthrough<Int>()
    private(set) weak var currentPageControl: UIPageControl?
    
    public init(transitionStyle: UIPageViewController.TransitionStyle = .scroll,
                navigationOrientation: UIPageViewController.NavigationOrientation = .horizontal,
                contentViewControllers: [UIViewController]) {
        
        self.pageViewController = UIPageViewController(transitionStyle: transitionStyle,
                                                  navigationOrientation: navigationOrientation,
                                                  options: nil)
        
        super.init(nibName: nil, bundle: nil)
        self.setPageViewController()
        self.set(contentViewControllers: contentViewControllers)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.pageViewController = UIPageViewController(transitionStyle: .scroll,
                                                  navigationOrientation: .horizontal,
                                                  options: nil)
        super.init(coder: aDecoder)
        self.setPageViewController()
    }
    
    public func set(contentViewControllers: [UIViewController]) {
        self.contentViewControllers = contentViewControllers
        
        if contentViewControllers.count > 0 {
            pageViewController.setViewControllers([contentViewControllers[0]], direction: .forward, animated: false, completion: nil)
        }
        
    }

    private func setPageViewController() {
        let pageView = pageViewController.view!
        pageView.translatesAutoresizingMaskIntoConstraints = false
        
        addChild(pageViewController)
        view.addSubview(pageView)
        
        NSLayoutConstraint.activate([
            pageView.topAnchor.constraint(equalTo: view.topAnchor),
            pageView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            pageView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            pageView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
        
        pageViewController.didMove(toParent: self)
        pageViewController.dataSource = self
        pageViewController.delegate = self
    }
    
    public func onPageSwitch<Binder: AnyObject>(binder: Binder, _ callback: @escaping (Binder, Int) -> Void) {
        onSwitch.observe(binder, with: callback)
    }
    
    public func createPageControl(withColor pageColor: UIColor, selectedColor: UIColor, startingIndex: Int = 0) -> UIPageControl {
        var pageControl: UIPageControl
        
        if let currentPageControl = self.currentPageControl {
            pageControl = currentPageControl
        } else {
            pageControl = UIPageControl()
            self.currentPageControl = pageControl
        }
        
        pageControl.pageIndicatorTintColor = pageColor
        pageControl.currentPageIndicatorTintColor = selectedColor
        pageControl.currentPage = startingIndex
        pageControl.numberOfPages = contentViewControllers?.count ?? 0
        
        return pageControl
    }
    
}

extension CKCPageViewController: UIPageViewControllerDelegate {
    
    public func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        guard let currentViewController = pageViewController.viewControllers?.first,
            let currentIndex = contentViewControllers?.firstIndex(of: currentViewController) else { return }
        
        currentPageControl?.currentPage = currentIndex
        onSwitch.callback?(currentIndex)
    }
    
}

extension CKCPageViewController: UIPageViewControllerDataSource {
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let contentCount = contentViewControllers?.count else { return nil }
        guard let index = contentViewControllers?.firstIndex(of: viewController) else { return nil }
        
        if index <= 0 {
            return hasContinuousScroll ? contentViewControllers?[contentCount - 1] : nil
        } else if index <= contentCount - 1 {
            return contentViewControllers?[index - 1]
        } else {
            return hasContinuousScroll ? contentViewControllers?[0] : nil
        }
    }
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let contentCount = contentViewControllers?.count else { return nil }
        guard let index = contentViewControllers?.firstIndex(of: viewController) else { return nil }

        if index < 0 {
            return hasContinuousScroll ? contentViewControllers?[contentCount - 1] : nil
        } else if index < contentCount - 1 {
            return contentViewControllers?[index + 1]
        } else {
            return hasContinuousScroll ? contentViewControllers?[0] : nil
        }
    }
    
}

