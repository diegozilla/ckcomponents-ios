//
//  CKCBottomSheetViewController+Heights.swift
//  CKCComponents
//
//  Created by Diego on 8/10/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

extension CKCBottomSheetViewController {
    
    var lowStateHeight: CGFloat {
        return State.low.heightPercentage * rootViewSafeAreaHeight
    }
    
    var midStateHeight: CGFloat {
        return State.mid.heightPercentage * rootViewSafeAreaHeight
    }
    
    var fullStateHeight: CGFloat {
        return State.full.heightPercentage * rootViewSafeAreaHeight
    }
    
    var maxHeightLimit: CGFloat {
        return fullStateHeight
    }
    
    var minHeightLimit: CGFloat {
        return lowStateHeight
    }
    
    var midHeightThreshold: CGFloat {
        let heightDiff = (midStateHeight - lowStateHeight) *  0.5
        return lowStateHeight + heightDiff
    }
    
    var fullHeightThreshold: CGFloat {
        let heightDiff = (fullStateHeight - midStateHeight) *  0.5
        return midStateHeight + heightDiff
    }
    
}
