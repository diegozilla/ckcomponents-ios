//
//  CKCBottomSheetViewController.swift
//  CKCComponents
//
//  Created by Diego on 8/9/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit
import CKFoundation

public typealias CKCBottomSheetContentViewController = UIViewController & CKCBottomSheetViewControllerObservable

public protocol CKCBottomSheetViewControllerObservable {
    var bottomSheetObserver: CKCBottomSheetViewControllerObserver? { get set }
}

public class CKCBottomSheetViewControllerObserver {
    public lazy var onStateChange = CKFMessage.Passthrough<CKCBottomSheetViewController.State>()
}

public class CKCBottomSheetViewController: UIViewController {
    
    private weak var contentViewController: CKCBottomSheetContentViewController?
    private weak var rootViewController: UIViewController?
    private let dragViewHeight: CGFloat = 25
    
    public var currentState: State = .low
    var currentStateHeight: CGFloat {
        return currentState.heightPercentage * rootViewSafeAreaHeight
    }
    
    private var heightConstraint: NSLayoutConstraint?
    private var dragAnimation: UIViewPropertyAnimator!
    
    private lazy var dragGesture: UIPanGestureRecognizer = {
        let gesture = UIPanGestureRecognizer()
        gesture.addTarget(self, action: #selector(self.dragView(gesture:)))
        return gesture
    }()
    
    private lazy var tapGesture: UITapGestureRecognizer = {
        let gesture = UITapGestureRecognizer()
        gesture.addTarget(self, action: #selector(self.tapView(gesture:)))
        return gesture
    }()
    
    private lazy var topDragView: CKCBottomSheetDraggingView = {
        let view = CKCBottomSheetDraggingView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    public var bottomSheetView: UIView {
        return view
    }
    
    private lazy var dragStateAnimator = UIViewPropertyAnimator(duration: 0.20, dampingRatio: 0.9, animations: nil)
    private lazy var tapStateAnimator = UIViewPropertyAnimator(duration: 0.45, dampingRatio: 0.75, animations: nil)

    var rootViewSafeAreaHeight: CGFloat {
        guard let rootViewController = self.rootViewController else { return 0 }
        return rootViewController.view.bounds.height - rootViewController.view.safeAreaInsets.top - rootViewController.view.safeAreaInsets.bottom
    }
    
    public init(contentViewController: CKCBottomSheetContentViewController) {
        self.contentViewController = contentViewController
        super.init(nibName: nil, bundle: nil)
        self.set(contentViewController: contentViewController)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("Cant implement directly from storyboard")
    }

    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if heightConstraint == nil {
            heightConstraint = view.heightAnchor.constraint(equalToConstant: rootViewSafeAreaHeight * currentState.heightPercentage)
            heightConstraint?.isActive = true
        }
    }
    
    @objc private func tapView(gesture: UITapGestureRecognizer) {
        switch currentState {
        case .low:
            currentState = .mid
            setTapStateAnimator()
        case .mid:
            currentState = .full
            setTapStateAnimator()
        case .full:
            currentState = .low
            setTapStateAnimator()
        }
    }
    
    @objc private func dragView(gesture: UIPanGestureRecognizer) {

        switch gesture.state {
        case .began:
            gesture.setTranslation(.zero, in: view)
            
        case .changed:
            let point = gesture.translation(in: view)
            let currentHeight = heightConstraint?.constant ?? 0
            let newHeight = currentHeight - point.y

            if newHeight > minHeightLimit && newHeight < maxHeightLimit {
                heightConstraint?.constant = newHeight
            } else if newHeight >= maxHeightLimit && heightConstraint?.constant != maxHeightLimit {
                heightConstraint?.constant = maxHeightLimit
            } else  if newHeight <= minHeightLimit && heightConstraint?.constant != minHeightLimit {
                heightConstraint?.constant = minHeightLimit
            }
            
            gesture.setTranslation(.zero, in: view)
            
        case .ended:
            
            let endingHeight = heightConstraint?.constant ?? 0
            let isBetweenLowAndMid = endingHeight > lowStateHeight && endingHeight < midStateHeight
            let isBetweenMidAndFull = endingHeight > midStateHeight && endingHeight < fullStateHeight

            if endingHeight < lowStateHeight {
                currentState = .low
                setDragStateAnimator()
            } else if isBetweenLowAndMid && endingHeight < midHeightThreshold {
                currentState = .low
                setDragStateAnimator()
            } else if isBetweenLowAndMid && endingHeight >= midHeightThreshold {
                currentState = .mid
                setDragStateAnimator()
            } else if isBetweenMidAndFull && endingHeight < fullHeightThreshold {
                currentState = .mid
                setDragStateAnimator()
            } else if isBetweenMidAndFull && endingHeight >= fullHeightThreshold {
                currentState = .full
                setDragStateAnimator()
            } else if endingHeight > fullStateHeight {
                currentState = .full
                setDragStateAnimator()
            }

            gesture.setTranslation(.zero, in: view)
            
        case .cancelled:

            currentState = .low
            setDragStateAnimator()
            
        default:
            break
        }
        
        
    }
}

extension CKCBottomSheetViewController {
    
    public enum State {
        case low, mid, full
        
        var heightPercentage: CGFloat {
            switch self {
            case .low: return 0.15
            case .mid: return 0.5
            case .full: return 0.9
            }
        }
    }
    
}

extension CKCBottomSheetViewController {
    
    public func set(contentViewController: UIViewController) {
        
        guard let contentViewController = self.contentViewController else { return }
        
        view.backgroundColor = UIColor.white
        
        addChild(contentViewController)
        view.addSubview(contentViewController.view)
        contentViewController.view.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(topDragView)
        
        NSLayoutConstraint.activate([
            topDragView.topAnchor.constraint(equalTo: view.topAnchor),
            topDragView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            topDragView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            topDragView.heightAnchor.constraint(equalToConstant: dragViewHeight)
            ])
        
        NSLayoutConstraint.activate([
            contentViewController.view.topAnchor.constraint(equalTo: topDragView.bottomAnchor),
            contentViewController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            contentViewController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            contentViewController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])
        
        contentViewController.didMove(toParent: self)

        topDragView.addGestureRecognizer(dragGesture)
        topDragView.addGestureRecognizer(tapGesture)
    }
    
    fileprivate func setTapStateAnimator() {
        
        tapStateAnimator.addAnimations {
            self.heightConstraint?.constant = self.currentStateHeight
            self.rootViewController?.view.layoutIfNeeded()
        }
        tapStateAnimator.startAnimation()
    }
    
    fileprivate func setDragStateAnimator() {
        
        dragStateAnimator.addAnimations {
            self.heightConstraint?.constant = self.currentStateHeight
            self.rootViewController?.view.layoutIfNeeded()
            //self.contentViewController?.bottomSheetObserver?.onStateChange.callback?(self.state)
        }
        
        dragStateAnimator.startAnimation()
    }
    
    public func attach(toRootViewController rootViewController: UIViewController, cornerRadius: CGFloat = 8) {
        
        self.rootViewController = rootViewController
        rootViewController.addChild(self)
        view.translatesAutoresizingMaskIntoConstraints = false
        rootViewController.view.addSubview(view)
        
        NSLayoutConstraint.activate([
            view.bottomAnchor.constraint(equalTo: rootViewController.view.safeAreaLayoutGuide.bottomAnchor),
            view.leadingAnchor.constraint(equalTo: rootViewController.view.leadingAnchor),
            view.trailingAnchor.constraint(equalTo: rootViewController.view.trailingAnchor),
        ])
        
        self.didMove(toParent: rootViewController)
        
        let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: [.topLeft, .topRight], cornerRadii: CGSize(width: cornerRadius, height: cornerRadius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        view.layer.mask = mask
    }
    
    public func setOnTop() {
        self.rootViewController?.view.bringSubviewToFront(view)
    }
    
}
