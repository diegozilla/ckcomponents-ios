//
//  CKCBottomSheetDraggingView.swift
//  CKCComponents
//
//  Created by Diego on 8/10/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

class CKCBottomSheetDraggingView: UIView {

    private lazy var indicatorView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .darkGray
        return view
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialize()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        indicatorView.layer.masksToBounds = true
        indicatorView.layer.cornerRadius = indicatorView.bounds.height * 0.5
    }
    
    private func initialize() {
        
        addSubview(indicatorView)
        
        NSLayoutConstraint.activate([
            indicatorView.centerXAnchor.constraint(equalTo: centerXAnchor),
            indicatorView.centerYAnchor.constraint(equalTo: centerYAnchor),
            indicatorView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.25),
            indicatorView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 0.1)
        ])
        
    }
    
}
