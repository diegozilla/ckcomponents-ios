//
//  CKCTabBarViewController.swift
//  CKCComponents
//
//  Created by Diego on 12/3/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

open class CKCTabBarViewController: UITabBarController {
    
    public typealias CKCTabBarViewController = UIViewController & CKCTabBarItemDisplayable
    
    public var itemsTintColor: UIColor {
        get {
            return view.tintColor
        }
        set {
            view.tintColor = newValue
        }
    }

    public func addViewControllers(_ viewControllers: [CKCTabBarViewController], animated: Bool = false) {
        viewControllers.forEach { $0.tabBarItem = $0.item }
        setViewControllers(viewControllers, animated: animated)
    }
    
}
