//
//  CKCTabBarContent.swift
//  CKCComponents
//
//  Created by Diego on 12/3/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

public protocol CKCTabBarItemDisplayable {
    
    var tabTitle: String { get }
    var tabIcon: UIImage? { get }
    var tabSelectedIcon: UIImage? { get }
    
    var item: UITabBarItem { get }
}

extension CKCTabBarItemDisplayable {
    
    public var tabSelectedIcon: UIImage? { return nil }
    
    public var item: UITabBarItem {
        return UITabBarItem(title: tabTitle, image: tabIcon, selectedImage: tabSelectedIcon)
    }
}
