//
//  CKCState.swift
//  CKCComponents
//
//  Created by Diego on 7/28/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import Foundation
import CKFoundation

public class CKCState<Value: Equatable> {
    
    public var value: Value {
        set { if observer.value != newValue { observer.value = newValue } }
        get { return observer.value }
    }
    fileprivate var observer: CKFObserver.Value<Value>
    
    public init(value: Value) {
        self.observer = CKFObserver.Value(value: value)
    }
    
    public func bind<Binder: AnyObject>(_ owner: Binder, _ callback: @escaping (Binder, Value) -> Void) {
        observer.observe(owner, with: callback)
    }
    
}
