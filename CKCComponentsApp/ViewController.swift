//
//  ViewController.swift
//  CKCComponentsApp
//
//  Created by Diego on 8/11/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit
import CKCComponents

class ViewController: UIViewController {

    var textState: CKCState<String?>!
    var textfield: CKCBorderedTextField!
    
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let x = CKCFlatButton(title: "LALA", backgroundColor: UIColor.blue)
        x.frame = CGRect(x: 50, y: 50, width: 200, height: 50)
        x.onTap(binder: self) { (self, button) in
            self.counter += 1
            if self.counter % 2 != 0 {
                self.textfield.set(borderWidth: 1, color: UIColor.blue)
            } else {
                self.textfield.set(borderWidth: 1, color: UIColor.red)
            }
            self.view.endEditing(true)
        }
        view.addSubview(x)
        
        // Do any additional setup after loading the view.
    
        let t = CKCBorderedTextField(text: "Hello there!", tint: UIColor.red)
        t.setPlaceholder("AHH General Kenobi", font: UIFont.boldSystemFont(ofSize: 15), color: UIColor.darkGray)
        //t.frame = CGRect(x: 50, y: 200, width: 200, height: 50)
        //t.set(lineWidth: 2, color: UIColor.red)
        view.addSubview(t)
        
        let t2 = UITextField()
        t2.borderStyle = .roundedRect
        t2.text = "HELLO THERE 2"
        t2.frame = CGRect(x: 50, y: 200, width: 200, height: 50)
        t2.frame.origin.y += 100
        view.addSubview(t2)
        //t.set(placeholder: "AHH General Kenobi", font: UIFont.boldSystemFont(ofSize: 15), color: UIColor.darkGray)
        
        t.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            t.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            t.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            t.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            t.heightAnchor.constraint(equalToConstant: 50)
        ])

        let seg = CKCSegmentedControl(options: ["Hello", "There", "Pepega"], font: UIFont.boldSystemFont(ofSize: 20), color: .systemPurple, tintColor: .systemBlue)
        
        seg.frame = CGRect(x: 100, y: 600, width: 250, height: 50)
        seg.selectedSegmentIndex =  0
        
        seg.onSelection(binder: self) { (self, value) in
            print(value)
        }
        
        view.addSubview(seg)
        
        
    }


}

