//
//  PageViewController.swift
//  CKCComponentsApp
//
//  Created by Diego on 9/1/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit
import CKCComponents

class BasePageViewController: UIViewController {
    
    var pageControl: UIPageControl!
    
    
    var redVC: UIViewController = {
        let viewController = UIViewController()
        viewController.view.backgroundColor = .red
        return viewController
    }()
    var blueVC: UIViewController = {
        let viewController = UIViewController()
        viewController.view.backgroundColor = .blue
        return viewController
    }()
    var greenVC: UIViewController = {
        let viewController = UIViewController()
        viewController.view.backgroundColor = .green
        return viewController
    }()
    
    lazy var pageViewController: CKCPageViewController = {
        let pageViewController = CKCPageViewController(transitionStyle: .scroll, navigationOrientation: .horizontal, contentViewControllers: [redVC, blueVC, greenVC])
        pageViewController.hasContinuousScroll = true
        return pageViewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addChild(pageViewController)
        pageViewController.view.translatesAutoresizingMaskIntoConstraints = false
        pageViewController.view.backgroundColor = .lightGray
        view.addSubview(pageViewController.view)
        
        NSLayoutConstraint.activate([
            pageViewController.view.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            pageViewController.view.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            pageViewController.view.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
            pageViewController.view.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
        ])
        
        pageViewController.didMove(toParent: self)
        
        pageControl = pageViewController.createPageControl(withColor: UIColor.darkGray, selectedColor: .white)
        pageControl.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(pageControl)
        
        pageControl.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50).isActive = true
        pageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        
        pageViewController.onPageSwitch(binder: self) { (self, index) in
            
            print("changed!")
            
        }
        

        self.view.bringSubviewToFront(pageControl)
    }
    
}

extension BasePageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if viewController == redVC {
            return blueVC
        } else if (viewController == blueVC) {
            return greenVC
        } else {
            return redVC
        }
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if viewController == greenVC {
            return blueVC
        } else if viewController == blueVC {
            return redVC
        } else {
            return greenVC
        }
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return 3
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
}
