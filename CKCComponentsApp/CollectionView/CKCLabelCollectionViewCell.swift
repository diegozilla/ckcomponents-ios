//
//  CKCLabelCollectionViewCell.swift
//  CKCComponentsApp
//
//  Created by Diego on 8/31/19.
//  Copyright © 2019 Diego. All rights reserved.
//

import UIKit

class CKCLabelCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var label: UILabel!
}
